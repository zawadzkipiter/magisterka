__author__ = 'peter'
from random import randint

import numpy as np

secret_sequence = 'hidden message hidden message'
secret_bites_sequence = ''

for char in secret_sequence:
    secret_bites_sequence += (bin(ord(char))[2:])

print(secret_bites_sequence)

queue = list(secret_bites_sequence)
fileIn = "D:\galkaL3.txt"
fileOut = "D:\galkaL3_output.txt"


def getx(packet):
    return int(packet.split(' ')[0])


def gety(packet):
    return int(packet.split(' ')[1])


def process_packet(packetX1, packetX2, packetX3):
    X1_X = getx(packetX1)
    X1_Y = gety(packetX1)

    X2_X = getx(packetX2)
    X2_Y = gety(packetX2)

    X3_X = getx(packetX3)
    X3_Y = gety(packetX3)

    if abs(X1_Y - X3_Y) > 2 & abs(X1_X - X3_X) > 1:
        a = np.array([[0, 1], [X3_X - X1_X, 1]])
        b = np.array([X1_Y - X3_Y, 0])

        x = np.linalg.solve(a, b)

        # here abs maybe

        if X3_X - X1_X < 0:
            x_fake = randint(X3_X - X1_X, 0)
        else:
            x_fake = randint(0, X3_X - X1_X)

        # print(x_fake)

        y_value_on_function = x[0] * x_fake + x[1]
        # print(y_value_on_function)

        new_x = X1_X + x_fake

        byte_to_code = queue.pop()
        if bool(byte_to_code):
            value_to_add = 1
        else:
            value_to_add = -1

        new_y = X3_Y + int(round(y_value_on_function)) + value_to_add

        # print "calculated:" + str(new_x) + " " + str(new_y)
        return str(new_x) + " " + str(new_y)

    return str(X2_X) + " " + str(X2_Y)

with open(fileIn) as f:
    content = f.readlines()

content.reverse()

content.pop()
X1 = None
X2 = None
X3 = None
isReal = True

f = open(fileOut, 'w')

while len(content) >= 3:

    if X1 is not None:
        X3 = X2
        X2 = X1
    X1 = content.pop()

    if X2 is None:
        X2 = X1
        X1 = content.pop()
        X3 = X2
        X2 = X1
        X1 = content.pop()

    if isReal:
        X2 = process_packet(X1, X2, X3)
        isReal = False
    else:
        isReal = True

    # print("\nX1:" + X1 + "X2:" + X2 + "\nX3:" + X3)
    print(str(getx(X3)) + " " + str(gety(X3)))
    f.write(str(getx(X3)) + " " + str(gety(X3)))
    f.write('\n')
f.close()
