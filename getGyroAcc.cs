	// gyro values 
	Int16 x = (Int16)((UInt16)(inputData[20] << 8) | inputData[21]);
	Int16 y = (Int16)((UInt16)(inputData[22] << 8) | inputData[23]);
	Int16 z = (Int16)((UInt16)(inputData[24] << 8) | inputData[25]);
	
	// pitch yaw roll acceleration
	Int16 pitch = (Int16)((UInt16)(inputData[14] << 8) | inputData[15]);
	Int16 yaw = (Int16)((UInt16)(inputData[16] << 8) | inputData[17]);
	Int16 roll = (Int16)((UInt16)(inputData[18] << 8) | inputData[19]);