import itertools

__author__ = 'peter'

secret_sequence = 'hidden message hidden message hidden'
secret_bites_sequence = ''

for char in secret_sequence:
    secret_bites_sequence += (bin(ord(char))[2:])

print(secret_bites_sequence)

queue = list(secret_bites_sequence)
fileIn = "D:\gyroX.txt"
fileOut = "D:\gyroXout.txt"


def getx(packet):
    return int(packet.split(' ')[0])


def gety(packet):
    return int(packet.split(' ')[1])


def process_packet(packetX1, packetX2, packetX3):
    X1_X = getx(packetX1)

    X2_X = getx(packetX2)

    X3_X = getx(packetX3)

    if abs(X1_X) < 1000:
        byte_to_code = ''
        for _ in itertools.repeat(None, 7):
            if len(queue) == 0:
                byte_to_code += '0'
            else:
                byte_to_code += str(queue.pop())
        print(str(byte_to_code) + '\n')
        value = int(byte_to_code, 2)

        if X3_X >= 0:
            new_x = X3_X - value
        else:
            new_x = X3_X + value

        return str(new_x)

    return str(X2_X)


with open(fileIn) as f:
    content = f.readlines()

content.reverse()

content.pop()
X1 = None
X2 = None
X3 = None
isReal = True

f = open(fileOut, 'w')

while len(content) >= 3:

    if X1 is not None:
        X3 = X2
        X2 = X1
    X1 = content.pop()

    if X2 is None:
        X2 = X1
        X1 = content.pop()
        X3 = X2
        X2 = X1
        X1 = content.pop()

    if isReal:
        X2 = process_packet(X1, X2, X3)
        isReal = False
    else:
        isReal = True

    # print("\nX1:" + X1 + "X2:" + X2 + "\nX3:" + X3)
    print(str(getx(X3)))
    f.write(str(getx(X3)))
    f.write('\n')
f.close()
